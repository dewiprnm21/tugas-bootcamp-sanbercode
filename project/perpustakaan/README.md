# Final Project

## Kelompok 8
## Anggota Kelompok

1) Dewi Purnamasari
2) Gilang Kumara
3) Aulia Riski Molanda

## Tema Project
Tema yang diangkat pada project ini adalah Sistem Informasi Perpustakaan. Dimana kami membuat sistem sederhana untuk proses yang terjadi di perpustakaan seperti data buku, data kategori buku, data peminjaman, dan data pengembalian.

## ERD 

![ERD.png](ERD.png?raw=true)

## Link Video
Link Demo Aplikasi : https://youtu.be/QtR4N550CRM
Link Deploy : https://laravel.com/docs/contributions#code-of-conduct



