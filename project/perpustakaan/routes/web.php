<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['auth']], function () {
    //CRUD Kategori
    Route::resource('kategori', 'kategoricontroller');

    //Profil
    Route::resource('profil', 'ProfilController')->only([
        'index', 'update'
    ]);

    //CRUDPeminjaman
    Route::resource('peminjaman', 'PeminjamanController');
});

// CRUD BUKU
Route::resource('buku', 'BukuController');

Auth::routes();

Route::get('/test-dompdf', function(){
    $pdf = App::make('dompdf.wrapper');
    $pdf->loadHTML('<h1>Hello World</h1>');
    return $pdf->stream();
});
