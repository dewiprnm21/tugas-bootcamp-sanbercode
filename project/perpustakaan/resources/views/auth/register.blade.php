@extends('layouts.master')

@section('content')

<body class="h-100">
    <div class="authincation h-100">
        <div class="container-fluid h-100">
            <div class="row justify-content-center h-50 align-items-center">
                <div class="col-md-6">
                    <div class="authincation-content">
                        <div class="row no-gutters">
                            <div class="col-xl-12">
                                <div class="auth-form">
                                    <h4 class="text-center mb-4">Registrasi Petugas Perpustakaan</h4>
                                    <form method="POST" action="{{ route('register') }}">
                                        @csrf

                                        <div class="form-group">
                                            <label><strong>Nama</strong></label>
                                            <input type="text" name="name" class="form-control" placeholder="Nama">
                                            
                                            @error('name')
                                                <div class="alert alert-danger">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label><strong>Email</strong></label>
                                            <input type="email" name="email" class="form-control" placeholder="hello@example.com">

                                            @error('email')
                                                <div class="alert alert-danger">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label><strong>Password</strong></label>
                                            <input type="password" name="password" class="form-control" value="Password">

                                            @error('password')
                                                <div class="alert alert-danger">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label><strong>Confirm Password</strong></label>
                                            <input type="password" name="password_confirmation" class="form-control" value="Password">
                                        </div>
                                        <div class="form-group">
                                            <label><strong>Alamat</strong></label>
                                            <textarea name="alamat" class="form-control"></textarea>
                                            @error('alamat')
                                                <div class="alert alert-danger">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label><strong>Nomor Telepon</strong></label>
                                            <input type="number" name="telp" class="form-control" placeholder="Nomor Telepon">
                                            
                                            @error('telp')
                                                <div class="alert alert-danger">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label><strong>Jenis Kelamin</strong></label>
                                            <input type="text" name="jenis_kelamin" class="form-control" placeholder="Jenis Kelamin">
                                            
                                            @error('jenis_kelamin')
                                                <div class="alert alert-danger">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                        <div class="text-center mt-4">
                                            <button type="submit" class="btn btn-primary btn-block">Registrasi</button>
                                        </div>
                                    </form>
                                    {{-- <div class="new-account mt-3">
                                        <p>Sudah mempunyai akun? <a class="text-primary" href="{{ route('login') }}">Masuk</a></p>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="{{ asset('/admin/vendor/global/global.min.js')}}"></script>
    <script src="{{ asset('/admin/js/quixnav-init.js')}}"></script>
    <!--endRemoveIf(production)-->
</body>

@endsection
