<div class="quixnav">
    <div class="quixnav-scroll">
        <ul class="metismenu" id="menu">
            @auth
              <li><div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                    <img src="{{ asset('/admin/images/avatar/1.png')}}" class="img-circle elevation-1" alt="User Image">
                    </div>
                    <div class="info">
                    <a href="#" class="d-block">{{ Auth::user()->name}} ({{Auth::user()->profil->alamat}})</a>
                    </div>
                </div>
              </li>
              <li class="nav-label first">Menu</li>
              <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i
                        class="icon icon-single-copy-06"></i><span class="nav-text">Perpustakaan</span></a>
                <ul aria-expanded="false">
                    <li><a href="/buku">Buku</a></li>
                    <li><a href="/kategori">Kategori</a></li>
                    {{-- <li><a href="/peminjaman">Peminjaman</a></li> --}}
                </ul>
             </li> 
             <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i
                                class="icon icon-single-04"></i><span class="nav-text">User</span></a>
                        <ul aria-expanded="false">
                            <li><a href="/register">Registrasi</a></li>
                            <li><a href="/profil">Profile</a></li>
                        </ul>
            </li>
            @endauth
            @guest
                    <li><a href="/buku">Buku</a></li>
            @endguest
        </ul>
    </div>
</div>