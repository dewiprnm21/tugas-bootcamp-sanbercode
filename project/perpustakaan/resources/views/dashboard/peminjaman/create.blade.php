@extends('layouts.master')

@section('title')
   Tambah Peminjaman
@endsection

@section('content')

    <form action="/peminjaman" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label>Nama</label><br> 
            <input type="file" class="form-control" name="nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label> Buku </label>
            <select name="buku_id" class="form-control" id="">
                <option value="">----Pilih Buku---</option>
                @foreach ($buku as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option> 
                @endforeach
            </select>
            @error('buku_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Tanggal Peminjaman</label>
            <input type="date" class="form-control" name="tanggal_pinjam">
            @error('tanggal_pinjam')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Tanggal Pengembalian</label>
            <input type="date" class="form-control" name="tanggal_balik">
            @error('tanggal_balik')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        

        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
    
@endsection