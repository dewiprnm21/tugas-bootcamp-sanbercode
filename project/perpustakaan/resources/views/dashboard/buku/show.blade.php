@extends('layouts.master')

@section('title')
   Halaman Detail Buku 
@endsection

@section('content')
    <img src="{{asset('image/'.$buku->thumbnail)}}" width="200px" alt="">
    <h2 class="text-primary">{{$buku->nama}}</h4>
    <p>{{$buku->deskripsi}}</p>
    <p> Tahun : {{$buku->tahun}}</p>
    <p> Penerbit : {{$buku->penerbit}}</p>
    <p> Pengarang : {{$buku->pengarang}}</p>
    
@endsection