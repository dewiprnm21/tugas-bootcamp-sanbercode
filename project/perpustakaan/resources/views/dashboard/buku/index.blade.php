@extends('layouts.master')

@section('title')
   List Buku
@endsection

@section('content')

@auth
  <a href="/buku/create" class="btn btn-success btn-sm my-2">Tambah Buku</a>   
@endauth


<div class="row">
  @forelse ($buku as $item)
      <div class="col-4">
        <div class="card">
          <img class="card-img-top" src="{{asset('image/'.$item->thumbnail)}}" height="10%" width="30%" alt="">
          <div class="card-body">
            <h5>{{$item->nama}}</h5>
              <span class="badge badge-info">{{$item->kategori->nama}}</span>
              <p class="card-text">{{ Str::limit($item->deskripsi, 100) }}</p>
              @auth
                <form action="{{route('buku.destroy', $item->id)}}" method="POST">
                  @csrf
                  @method('delete')

                  <a href="{{route('buku.show', $item->id)}}" class="btn btn-primary btn-sm">Detail</a>
                <a href="{{route('buku.edit', $item->id)}}" class="btn btn-warning btn-sm">Edit</a>
                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form> 
              @endauth
              @guest
                <a href="{{route('buku.show', $item->id)}}" class="btn btn-primary btn-sm">Detail</a>
              @endguest
          </div>
        </div>
      </div>
  @empty
      <h1> Tidak ada Buku yang ditemukan</h1>
  @endforelse
</div>
@endsection

@push('scripts')
  <script>
    Swal.fire({
        title: "Berhasil!",
        text: "Memasangkan script sweet alert",
        icon: "success",
        confirmButtonText: "Cool",
    });
  </script>
@endpush