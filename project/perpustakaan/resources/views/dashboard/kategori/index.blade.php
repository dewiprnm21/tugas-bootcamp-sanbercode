@extends('layouts.master')

@section('title')
   List Kategori
@endsection

@section('content')

<a href="/kategori/create" class="btn btn-success btn-sm my-2">Tambah</a>

<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($kategori as $key=> $item)
          <tr>
              <td>{{$key + 1 }}</td>
              <td>{{$item->nama}}</td>
              <td>
                <form action="/kategori/{{$item->id}}" method="post">
                  @csrf
                  @method('delete')
                    <a href="/kategori/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                    <a href="/kategori/{{$item->id}}/edit" class="btn btn-success btn-sm">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="delete">
                  </form>
              </td>
          </tr>
      @empty
          <tr>
              <td>Data tidak ditemukan</td>
          </tr>
      @endforelse
    </tbody>
  </table>
@endsection