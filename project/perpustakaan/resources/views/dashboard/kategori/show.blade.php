@extends('layouts.master')

@section('title')
   List kategori {{$kategori->nama}}
@endsection

@section('content')
    {{-- <h1>{{$kategori->nama}}</h1> --}}
    {{-- <p>{{$kategori->nama}}</p> --}}

    @forelse ($kategori->buku as $item)
    <div class="col-4">
        <div class="card">
          <img class="card-img-top" src="{{asset('image/'.$item->thumbnail)}}" height="10%" width="30%" alt="">
          <div class="card-body">
            <h5>{{$item->nama}}</h5>
              <span class="badge badge-info">{{$item->kategori->nama}}</span>
              <p class="card-text">{{ Str::limit($item->deskripsi, 100) }}</p>
                  <a href="{{route('buku.show', $item->id)}}" class="btn btn-primary btn-sm">Detail</a>
          </div>
        </div>
    </div>
    @empty
        <p>Tidak ada buku di kategori ini</p>
    @endforelse

@endsection