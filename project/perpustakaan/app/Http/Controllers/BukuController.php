<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// use Illuminate\Support\Facades\DB;

use App\Buku;
use App\Kategori;
use File;

class BukuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index','show');
    }

    public function create()
    {
        $kategori = Kategori::all();

        return view('dashboard.buku.create', compact('kategori'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'thumbnail' => 'mimes:png,jpg,jpeg',
            'nama' => 'required|max:255',
            'deskripsi' => 'required',
            'tahun' => 'required',
            'penerbit' => 'required',
            'pengarang' => 'required',
            'kategori_id' => 'required',
        ]);

        $fileName = time().'.'.$request->thumbnail->extension();
        
        $kategori = new Buku;
        $kategori->thumbnail = $fileName;
        $kategori->nama = $request->nama;
        $kategori->deskripsi = $request->deskripsi;
        $kategori->tahun = $request->tahun;
        $kategori->penerbit = $request->penerbit;
        $kategori->pengarang = $request->pengarang;
        $kategori->kategori_id = $request->kategori_id;
    
        $kategori->save();

        $request->thumbnail->move(public_path('image'), $fileName);
        
        return redirect('/buku');
    }

    public function index()
    {
        $buku = Buku::all();

        return view('dashboard.buku.index', compact('buku'));
    }

    public function show($id)
    {
        $buku = Buku::findorfail($id);

        return view('dashboard.buku.show', compact('buku'));
    }

    public function edit($id)
    {
        $kategori = Kategori::all();
        $buku = Buku::findorfail($id);
        return view('dashboard.buku.edit', compact('buku', 'kategori'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'thumbnail' => 'required|mimes:png,jpg,jpeg',
            'nama' => 'required|max:255',
            'deskripsi' => 'required',
            'tahun' => 'required',
            'penerbit' => 'required',
            'pengarang' => 'required',
            'kategori_id' => 'required',
        ]);

        $buku = Buku::findorfail($id);
        // $post->delete();
        if($request->has('thumbnail')){
            $path = "image/";
            File::delete($path . $buku->thumbnail);
            $fileName = time().'.'.$request->thumbnail->extension();

            $request->thumbnail->move(public_path('image'), $fileName);

            $buku_data = [
                'thumbnail' => $fileName,
                'nama' => $request->nama,
                'deskripsi' => $request->deskripsi,
                'tahun' => $request->tahun,
                'penerbit' => $request->penerbit,
                'pengarang' => $request->pengarang,
                'kategori_id' => $request->kategori_id,
            ];
        } else{
            $buku_data = [
                'nama' => $request->nama,
                'deskripsi' => $request->deskripsi,
                'tahun' => $request->tahun,
                'penerbit' => $request->penerbit,
                'pengarang' => $request->pengarang,
                'kategori_id' => $request->kategori_id,
            ];
        }

        $buku->update($buku_data);

        return redirect('/buku');
    }

    public function destroy($id)
    {
        $buku = Buku::findorfail($id);
        $path = "image/";
        File::delete($path . $buku->thumbnail);
        $buku->delete();

        return redirect('/buku');

    }
}