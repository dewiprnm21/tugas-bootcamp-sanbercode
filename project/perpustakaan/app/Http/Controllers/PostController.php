<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\postsExport;
use Maatwebsite\Excel\Facades\Excel;

class PostController extends Controller
{
    public function export() 
    {
        return Excel::download(new postsExport, 'posts.xlsx');
    }
}
