<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> FORM SIGN UP </title>
</head>
<body>
    <H1> Buat Account Baru! </H1>

    <h3> Sign Up Form </h3>
    <form action="/signup" Method="POST">
        @csrf
        <label> First Name: </label> <br><br>
            <input type="text" name="firstname"> <br><br>
        
        <label> Last Name: </label> <br><br>
            <input type="text" name="lastname"> <br><br>
        
        <label> Gender: </label> <br><br>
            <input type="radio" name="Gender" value="MALE"> Male <br>
            <input type="radio" name="Gender" value="FEMALE"> Female <br>
            <input type="radio" name="Gender" value="OTHER"> Other <br><br>
        
        <label> Nationality: </label> <br><br>
            <select name="nationality">
                <option value="indonesia"> Indonesia </option>
                <option value="jerman"> Jerman </option>
                <option value="jepang"> Jepang </option>
            </select> 
            <br><br>

        <label> Language Spoken: </label> <br><br>
            <input type="checkbox" name="language spoken" value="Bahasa Indonesia"> Bahasa Indonesia <br>
            <input type="checkbox" name="language spoken" value="English"> English <br>
            <input type="checkbox" name="language spoken" value="Other"> Other <br><br>

        <label> Bio: </label> <br><br>
            <textarea name="bio" cols="30" rows="10"> </textarea> <br>

            <input type="submit" name="Sign Up" value="Sign Up">
    </form>

</body>
</html>